#!/bin/bash
# Galaxy S8 Modpack for Linux v1.10 by djb77 / XDA-Developers

export rootdir=$(pwd)
export version=$(<tools/version)
export zipname='Galaxy_S8_ModPack_v'$version'.zip'

echo ""
echo "Galaxy S8 Modpack build script by @djb77"
echo "----------------------------------------"
echo ""

echo ""
echo "Building Zip File $zipname"
cd $rootdir/build
zip -9gq $zipname -r META-INF/ -x "*~"
zip -9gq $zipname -r modpack/ -x "*~"

if [ -n `which java` ]; then
	echo "Java detected, signing zip ..."
	mv $zipname old$zipname
	java -Xmx4096m -jar $rootdir/tools/signapk.jar -w $rootdir/tools/testkey.x509.pem $rootdir/tools/testkey.pk8 old$zipname $zipname
	rm old$zipname
fi
chmod a+r $zipname
mv $zipname $rootdir/$zipname
cd $rootdir

echo ""
echo "Done."
echo ""

