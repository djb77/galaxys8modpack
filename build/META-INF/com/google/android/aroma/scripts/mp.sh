#!/sbin/sh
# --------------------------------
# S8 MODPACK AROMA INSTALLER v1.10
# mp.sh portion
#
# DO NOT USE ANY PORTION OF THIS
# CODE WITHOUT MY PERMISSION!!
# --------------------------------

# Read option number from updater-script
OPTION=$1

# Variables
AROMA=/tmp/aroma

if [ $OPTION == "ff" ]; then
	sed -i -- 's/CALCULATOR_SUPPORT_CONVERTER>FALSE/CALCULATOR_SUPPORT_CONVERTER>TRUE/g' /system/etc/floating_feature.xml
	exit 10
fi

if [ $OPTION == "apk_patch" ]; then
	## APK Patching Scripts
	# Patching Script
	FUNC_PATCH()
	{
	mkdir /tmp/work
	cp -f $APKLocation/$APKName/$APKName.apk /tmp/apk_patch/$APKName.zip
	cd $APKPatch
	$AROMA/zip -r -0 /tmp/apk_patch/$APKName.zip *
	rm -rf $APKLocation/$APKName/$APKName.apk
	cp -f /tmp/apk_patch/$APKName.zip $APKLocation/$APKName/$APKName.apk
	rm -rf /tmp/work
	}
	# Patching Script for framework-res
	FUNC_PATCH_FRAMEWORK()
	{
	mkdir /tmp/work
	cp -f $APKLocation/$APKName.apk /tmp/apk_patch/$APKName.zip
	cd $APKPatch
	$AROMA/zip -r -0 /tmp/apk_patch/$APKName.zip *
	rm -rf $APKLocation/$APKName/$APKName.apk
	cp -f /tmp/apk_patch/$APKName.zip $APKLocation/$APKName.apk
	rm -rf /tmp/work
	}
	# Patch framework-res
	APKLocation=/system/framework
	APKName=framework-res
	APKPatch=/tmp/apk_patch/framework-res
	[ -d /tmp/apk_patch/framework-res ] && FUNC_PATCH_FRAMEWORK
	# Patch samsung-framework-res
	APKLocation=/system/framework
	APKName=samsung-framework-res
	APKPatch=/tmp/apk_patch/samsung-framework-res
	[ -d /tmp/apk_patch/samsung-framework-res ] && FUNC_PATCH
	# Patch SamsungCamera6 
	APKLocation=/system/priv-app
	APKName=SamsungCamera6
	APKPatch=/tmp/apk_patch/SamsungCamera6
	[ -d /tmp/apk_patch/SamsungCamera6 ] && FUNC_PATCH
	# Patch SecMyFiles2017
	APKLocation=/system/priv-app 
	APKName=SecMyFiles2017
	APKPatch=/tmp/apk_patch/SecMyFiles2017
	[ -d /tmp/apk_patch/SecMyFiles2017 ] && FUNC_PATCH
	# Patch SecSettings2
	APKLocation=/system/priv-app
	APKName=SecSettings2
	APKPatch=/tmp/apk_patch/SecSettings2
	[ -d /tmp/apk_patch/SecSettings2 ] && FUNC_PATCH
	# Patch SecSetupWizard2015
	APKLocation=/system/priv-app
	APKName=SecSetupWizard2015
	APKPatch=/tmp/apk_patch/SecSetupWizard2015
	[ -d /tmp/apk_patch/SecSetupWizard2015 ] && FUNC_PATCH
	# Patch SystemUI
	APKLocation=/system/priv-app
	APKName=SystemUI
	APKPatch=/tmp/apk_patch/SystemUI
	[ -d /tmp/apk_patch/SystemUI ] && FUNC_PATCH
	exit 10
fi

